# Visma Digital Commerce: AkeneoIntegration

This module is responsible for creating an integration between Akeneo PIM and Magento 2.

![Example image](doc/images/example.png)

## Functionality

## Technical info

PHP documentation is available [here](doc/phpdoc/index.html).

## License

This module is proprietary software belonging to Visma Digital Commerce AS.

<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Publisher;

use Magento\AsynchronousOperations\Api\Data\OperationInterfaceFactory;
use Magento\Framework\Bulk\BulkManagementInterface;
use Magento\Framework\Bulk\OperationInterface;
use Magento\Framework\DataObject\IdentityGeneratorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json;
use Visma\AkeneoIntegration\Api\PublisherInterface;
use Visma\AkeneoIntegration\Logger\AkeneoIntegrationLogger;

class Publisher implements PublisherInterface
{
    /**
     * @var IdentityGeneratorInterface $identityGenerator
     */
    private IdentityGeneratorInterface $identityGenerator;

    /**
     * @var OperationInterfaceFactory $operationFactory
     */
    private OperationInterfaceFactory $operationFactory;

    /**
     * @var BulkManagementInterface $bulkManagement
     */
    private BulkManagementInterface $bulkManagement;

    /**
     * @var AkeneoIntegrationLogger $akeneoIntegrationLogger
     */
    private AkeneoIntegrationLogger $akeneoIntegrationLogger;

    /**
     * @var Json $json
     */
    private Json $json;

    /**
     * @param IdentityGeneratorInterface $identityGenerator
     * @param OperationInterfaceFactory $operationFactory
     * @param BulkManagementInterface $bulkManagement
     * @param AkeneoIntegrationLogger $akeneoIntegrationLogger
     * @param Json $json
     */
    public function __construct(
        IdentityGeneratorInterface $identityGenerator,
        OperationInterfaceFactory $operationFactory,
        BulkManagementInterface $bulkManagement,
        AkeneoIntegrationLogger $akeneoIntegrationLogger,
        Json $json
    ) {
        $this->identityGenerator = $identityGenerator;
        $this->operationFactory = $operationFactory;
        $this->bulkManagement = $bulkManagement;
        $this->akeneoIntegrationLogger = $akeneoIntegrationLogger;
        $this->json = $json;
    }

    /**
     * @inheritDoc
     */
    public function execute(array $data, string $topicName): void
    {
        $operation = $this->createOperation($data, $topicName);

        $result = $this->bulkManagement->scheduleBulk($operation->getBulkUuid(), [$operation], 'Processing');

        $this->akeneoIntegrationLogger->debug(
            "Published operation with topic name: $topicName, UUID: {$operation->getBulkUuid()}"
        );

        if (!$result) {
            throw new LocalizedException(
                __('Something went wrong while processing the request.')
            );
        }
    }

    /**
     * @param array $data
     * @param string $topicName
     * @return OperationInterface
     */
    private function createOperation(array $data, string $topicName): OperationInterface
    {
        return $this->operationFactory->create()
            ->setBulkUuid($this->identityGenerator->generateId())
            ->setTopicName($topicName)
            ->setSerializedData($this->json->serialize($data))
            ->setStatus(OperationInterface::STATUS_TYPE_OPEN);
    }
}

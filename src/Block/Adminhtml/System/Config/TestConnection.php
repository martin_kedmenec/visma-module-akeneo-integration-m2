<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Block\Adminhtml\System\Config;

use Magento\Backend\Block\Widget\Button;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class TestConnection extends Field
{
    /**
     * @inheirtDoc
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function _getElementHtml(AbstractElement $element): string
    {
        $url = $this->getUrl('akeneo_integration/test_connection');

        return $this->getLayout()->createBlock(Button::class)->setData([
            'id' => 'test_connection',
            'label' => __('Test'),
            'onclick' => "setLocation('$url')"
        ])->toHtml();
    }
}

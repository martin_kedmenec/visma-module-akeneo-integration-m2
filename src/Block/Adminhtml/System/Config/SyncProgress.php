<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Block\Adminhtml\System\Config;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class SyncProgress extends Field
{
    protected $_template = 'Visma_AkeneoIntegration::system/config/sync_progress.phtml';

    /**
     * @inheirtDoc
     */
    protected function _getElementHtml(AbstractElement $element): string
    {
        return $this->_toHtml();
    }
}

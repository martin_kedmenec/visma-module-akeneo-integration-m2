<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Block\Adminhtml\System\Config;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\View\Helper\SecureHtmlRenderer;
use Visma\AkeneoIntegration\Helper\Config;

class ModuleVersion extends Field
{
    /**
     * @inheirtDoc
     */
    protected $_template = 'Visma_AkeneoIntegration::system/config/module_version.phtml';

    /**
     * @var Config $config
     */
    private Config $config;

    /**
     * @param Context $context
     * @param Config $config
     * @param SecureHtmlRenderer|null $secureRenderer
     * @param array $data
     */
    public function __construct(
        Context $context,
        Config $config,
        ?SecureHtmlRenderer $secureRenderer = null,
        array $data = []
    ) {
        parent::__construct($context, $data, $secureRenderer);
        $this->config = $config;
    }

    /**
     * @inheirtDoc
     */
    protected function _getElementHtml(AbstractElement $element): string
    {
        return $this->_toHtml();
    }

    /**
     * @return string
     */
    public function getModuleVersion(): string
    {
        return $this->config->getModuleVersion();
    }
}

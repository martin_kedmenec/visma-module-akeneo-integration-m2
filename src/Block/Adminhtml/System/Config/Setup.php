<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Block\Adminhtml\System\Config;

use Magento\Backend\Block\Widget\Button;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Setup extends Field
{
    /**
     * @inheirtDoc
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function _getElementHtml(AbstractElement $element): string
    {
        $url = $this->getUrl('visma_akeneo_integration/setup');

        return $this->getLayout()->createBlock(Button::class)->setData([
            'id' => 'start_setup',
            'label' => __('Start'),
            'onclick' => "setLocation('$url')"
        ])->toHtml();
    }
}

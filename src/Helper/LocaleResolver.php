<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Helper;

use Magento\Store\Api\StoreConfigManagerInterface;

class LocaleResolver
{
    /**
     * @var StoreConfigManagerInterface $storeConfigManager
     */
    private StoreConfigManagerInterface $storeConfigManager;

    /**
     * @param StoreConfigManagerInterface $storeConfigManager
     */
    public function __construct(
        StoreConfigManagerInterface $storeConfigManager
    ) {
        $this->storeConfigManager = $storeConfigManager;
    }

    /**
     * @param string $locale
     * @return int|null
     */
    public function getStoreIdByLocale(string $locale): ?int
    {
        foreach ($this->storeConfigManager->getStoreConfigs() as $storeConfig) {
            if ($storeConfig->getLocale() === $locale) {
                return (int)$storeConfig->getId();
            }
        }

        return null;
    }
}

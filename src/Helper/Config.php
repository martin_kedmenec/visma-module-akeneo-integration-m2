<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Module\ModuleListInterface;

class Config
{
    public const MODULE_NAME = 'Visma_AkeneoIntegration';

    private const XML_PATH_AKENEO_INTEGRATION_GENERAL_ENABLED = 'akeneo_integration/general/enabled';

    private const XML_PATH_AKENEO_INTEGRATION_CONNECTION_BASE_URL = 'akeneo_integration/connection/base_url';

    private const XML_PATH_AKENEO_INTEGRATION_CONNECTION_CLIENT_ID = 'akeneo_integration/connection/client_id';

    private const XML_PATH_AKENEO_INTEGRATION_CONNECTION_SECRET = 'akeneo_integration/connection/secret';

    private const XML_PATH_AKENEO_INTEGRATION_CONNECTION_USERNAME = 'akeneo_integration/connection/username';

    private const XML_PATH_AKENEO_INTEGRATION_CONNECTION_PASSWORD = 'akeneo_integration/connection/password';

    private const XML_PATH_AKENEO_INTEGRATION_EVENT_SUBSCRIPTION_SECRET = 'akeneo_integration/connection/secret';

    /**
     * @var ScopeConfigInterface $scopeConfig
     */
    private ScopeConfigInterface $scopeConfig;

    /**
     * @var ModuleListInterface $moduleList
     */
    private ModuleListInterface $moduleList;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param ModuleListInterface $moduleList
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ModuleListInterface $moduleList
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->moduleList = $moduleList;
    }

    public function getModuleVersion(): string
    {
        return (string)$this->moduleList->getOne(self::MODULE_NAME)['setup_version'];
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(self::XML_PATH_AKENEO_INTEGRATION_GENERAL_ENABLED);
    }

    /**
     * @return string
     */
    public function getConnectionBaseUrl(): string
    {
        return (string)$this->scopeConfig->getValue(self::XML_PATH_AKENEO_INTEGRATION_CONNECTION_BASE_URL);
    }

    /**
     * @return string
     */
    public function getConnectionClientId(): string
    {
        return (string)$this->scopeConfig->getValue(self::XML_PATH_AKENEO_INTEGRATION_CONNECTION_CLIENT_ID);
    }

    /**
     * @return string
     */
    public function getConnectionSecret(): string
    {
        return (string)$this->scopeConfig->getValue(self::XML_PATH_AKENEO_INTEGRATION_CONNECTION_SECRET);
    }

    /**
     * @return string
     */
    public function getConnectionUsername(): string
    {
        return (string)$this->scopeConfig->getValue(self::XML_PATH_AKENEO_INTEGRATION_CONNECTION_USERNAME);
    }

    /**
     * @return string
     */
    public function getConnectionPassword(): string
    {
        return (string)$this->scopeConfig->getValue(self::XML_PATH_AKENEO_INTEGRATION_CONNECTION_PASSWORD);
    }

    /**
     * @return string
     */
    public function getEventSubscriptionSecret(): string
    {
        return (string)$this->scopeConfig->getValue(self::XML_PATH_AKENEO_INTEGRATION_EVENT_SUBSCRIPTION_SECRET);
    }
}

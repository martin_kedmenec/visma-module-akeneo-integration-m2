<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Helper;

use Visma\AkeneoIntegration\Api\Data\AkeneoConstantsInterface;

class AttributeTypeMapper implements AkeneoConstantsInterface
{
    /**
     * @param string $akeneoAttributeType
     * @return string|null
     */
    public function getFrontendInputTypeByAkeneoAttributeType(string $akeneoAttributeType): ?string
    {
        foreach (self::AKENEO_MAGENTO_ATTRIBUTE_TYPE_MAPPING as $akeneoAttribute => $magentoAttribute) {
            if ($akeneoAttribute === $akeneoAttributeType) {
                return $magentoAttribute;
            }
        }

        return null;
    }
}

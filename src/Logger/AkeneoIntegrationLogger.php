<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Logger;

use Visma\Log\LoggerTrait;

class AkeneoIntegrationLogger
{
    use LoggerTrait;

    /**
     * @inheritDoc
     */
    protected static function getName(): string
    {
        return 'akeneo-integration';
    }
}

<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Api;

use Visma\AkeneoIntegration\Api\Data\UpdatedEventInterface;

interface EventsProcessorInterface
{
    /**
     * @param \Visma\AkeneoIntegration\Api\Data\UpdatedEventInterface[] $events
     * @return \Visma\AkeneoIntegration\Api\Data\UpdatedEventInterface
     */
    public function process(array $events): UpdatedEventInterface;
}

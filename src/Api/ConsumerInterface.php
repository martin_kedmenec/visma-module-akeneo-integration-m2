<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Api;

use Magento\AsynchronousOperations\Api\Data\OperationInterface;

interface ConsumerInterface
{
    /**
     * @param OperationInterface $operation
     * @return void
     */
    public function process(OperationInterface $operation): void;
}

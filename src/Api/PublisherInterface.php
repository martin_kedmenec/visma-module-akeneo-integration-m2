<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Api;

use Magento\Framework\Exception\LocalizedException;

interface PublisherInterface
{
    /**
     * @param array $data
     * @param string $topicName
     * @return void
     * @throws LocalizedException
     */
    public function execute(array $data, string $topicName): void;
}

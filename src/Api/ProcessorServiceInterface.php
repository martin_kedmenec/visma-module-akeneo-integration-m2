<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Api;

interface ProcessorServiceInterface
{
    /**
     * The main link between the Akeneo REST API and the message queue. This method fetches the data from the Akeneo
     * API and publishes it on the message exchange.
     *
     * @return void
     */
    public function process(): void;
}

<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * @api
 */
interface QuantifiedAssociationsInterface extends ExtensibleDataInterface
{
    public const PRODUCTS = 'products';

    public const PRODUCT_MODELS = 'product_models';

    /**
     * @return ProductInterface[]|null
     */
    public function getProducts(): ?array;

    /**
     * @return ProductModelsInterface[]|null
     */
    public function getProductModels(): ?array;

    /**
     * @param ProductInterface[]|null $products
     * @return QuantifiedAssociationsInterface
     */
    public function setProducts(?array $products): QuantifiedAssociationsInterface;

    /**
     * @param ProductModelsInterface[]|null $productModels
     * @return QuantifiedAssociationsInterface
     */
    public function setProductModels(?array $productModels): QuantifiedAssociationsInterface;
}

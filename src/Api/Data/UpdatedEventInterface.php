<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * @api
 */
interface UpdatedEventInterface extends ExtensibleDataInterface
{
    public const ACTION = 'action';

    public const EVENT_ID = 'event_id';

    public const EVENT_DATETIME = 'event_datetime';

    public const AUTHOR = 'author';

    public const AUTHOR_TYPE = 'author_type';

    public const PIM_SOURCE = 'pim_source';

    public const DATA = 'data';

    /**
     * @return string
     */
    public function getAction(): string;

    /**
     * @return string
     */
    public function getEventId(): string;

    /**
     * @return string
     */
    public function getEventDatetime(): string;

    /**
     * @return string
     */
    public function getAuthor(): string;

    /**
     * @return string
     */
    public function getAuthorType(): string;

    /**
     * @return string
     */
    public function getPimSource(): string;

    /**
     * @return \Visma\AkeneoIntegration\Api\Data\UpdatedDataInterface
     */
    public function getData(): UpdatedDataInterface;

    /**
     * @param string $action
     * @return UpdatedEventInterface
     */
    public function setAction(string $action): UpdatedEventInterface;

    /**
     * @param string $eventId
     * @return UpdatedEventInterface
     */
    public function setEventId(string $eventId): UpdatedEventInterface;

    /**
     * @param string $eventDatetime
     * @return UpdatedEventInterface
     */
    public function setEventDatetime(string $eventDatetime): UpdatedEventInterface;

    /**
     * @param string $author
     * @return UpdatedEventInterface
     */
    public function setAuthor(string $author): UpdatedEventInterface;

    /**
     * @param string $authorType
     * @return UpdatedEventInterface
     */
    public function setAuthorType(string $authorType): UpdatedEventInterface;

    /**
     * @param string $pimSource
     * @return UpdatedEventInterface
     */
    public function setPimSource(string $pimSource): UpdatedEventInterface;

    /**
     * @param \Visma\AkeneoIntegration\Api\Data\UpdatedDataInterface $data
     * @return UpdatedEventInterface
     */
    public function setData(UpdatedDataInterface $data): UpdatedEventInterface;
}

<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * @api
 */
interface UpdatedDataInterface extends ExtensibleDataInterface
{
    public const RESOURCE = 'resource';

    /**
     * @return \Visma\AkeneoIntegration\Api\Data\UpdatedResourceInterface
     */
    public function getResource(): UpdatedResourceInterface;

    /**
     * @param \Visma\AkeneoIntegration\Api\Data\UpdatedResourceInterface $resource
     * @return UpdatedDataInterface
     */
    public function setResource(UpdatedResourceInterface $resource): UpdatedDataInterface;
}

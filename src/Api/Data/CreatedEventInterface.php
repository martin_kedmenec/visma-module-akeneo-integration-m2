<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface CreatedEventInterface extends ExtensibleDataInterface
{
}

<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * @api
 */
interface ProductInterface extends ExtensibleDataInterface
{
    public const IDENTIFIER = 'identifier';

    public const QUANTITY = 'quantity';

    /**
     * @return string
     */
    public function getIdentifier(): string;

    /**
     * @return int
     */
    public function getQuantity(): int;

    /**
     * @param string $identifier
     * @return ProductInterface
     */
    public function setIdentifier(string $identifier): ProductInterface;

    /**
     * @param int $quantity
     * @return ProductInterface
     */
    public function setQuantity(int $quantity): ProductInterface;
}

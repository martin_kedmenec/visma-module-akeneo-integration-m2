<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * @api
 */
interface EventsInterface extends ExtensibleDataInterface
{
    public const EVENTS = 'events';

    /**
     * @return \Visma\AkeneoIntegration\Api\Data\UpdatedEventInterface[]
     */
    public function getEvents(): array;

    /**
     * @param \Visma\AkeneoIntegration\Api\Data\UpdatedEventInterface[] $events
     * @return EventsInterface
     */
    public function setEvents(array $events): EventsInterface;
}

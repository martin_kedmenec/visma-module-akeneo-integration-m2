<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * @api
 */
interface UpdatedResourceInterface extends ExtensibleDataInterface
{
    public const IDENTIFIER = 'code';

    public const ENABLED = 'enabled';

    public const FAMILY = 'family';

    public const CATEGORIES = 'categories';

    public const GROUPS = 'groups';

    public const PARENT = 'parent';

    public const VALUES = 'values';

    public const CREATED = 'created';

    public const UPDATED = 'updated';

    public const ASSOCIATIONS = 'associations';

    public const QUANTIFIED_ASSOCIATIONS = 'quantified_associations';

    public const METADATA = 'metadata';

    /**
     * @return string
     */
    public function getIdentifier(): string;

    /**
     * @return bool
     */
    public function isEnabled(): bool;

    /**
     * @return string
     */
    public function getFamily(): string;

    /**
     * @return string[]
     */
    public function getCategories(): array;

    /**
     * @return string[]|null
     */
    public function getGroups(): ?array;

    /**
     * @return string|null
     */
    public function getParent(): ?string;

    /**
     * @return mixed
     */
    public function getValues();

    /**
     * @return string
     */
    public function getCreated(): string;

    /**
     * @return string
     */
    public function getUpdated(): string;

    /**
     * @return \Visma\AkeneoIntegration\Api\Data\AssociationsInterface[]
     */
    public function getAssociations(): array;

    /**
     * @return \Visma\AkeneoIntegration\Api\Data\QuantifiedAssociationsInterface
     */
    public function getQuantifiedAssociations(): QuantifiedAssociationsInterface;

    /**
     * @return \Visma\AkeneoIntegration\Api\Data\MetadataInterface
     */
    public function getMetadata(): MetadataInterface;

    /**
     * @param string $identifier
     * @return UpdatedResourceInterface
     */
    public function setIdentifier(string $identifier): UpdatedResourceInterface;

    /**
     * @param bool $enabled
     * @return UpdatedResourceInterface
     */
    public function setEnabled(bool $enabled): UpdatedResourceInterface;

    /**
     * @param string $family
     * @return UpdatedResourceInterface
     */
    public function setFamily(string $family): UpdatedResourceInterface;

    /**
     * @param string[] $categories
     * @return UpdatedResourceInterface
     */
    public function setCategories(array $categories): UpdatedResourceInterface;

    /**
     * @param string[]|null $groups
     * @return UpdatedResourceInterface
     */
    public function setGroups(?array $groups): UpdatedResourceInterface;

    /**
     * @param string|null $parent
     * @return UpdatedResourceInterface
     */
    public function setParent(?string $parent): UpdatedResourceInterface;

    /**
     * @param mixed $values
     * @return UpdatedResourceInterface
     */
    public function setValues($values): UpdatedResourceInterface;

    /**
     * @param string $created
     * @return UpdatedResourceInterface
     */
    public function setCreated(string $created): UpdatedResourceInterface;

    /**
     * @param string $updated
     * @return UpdatedResourceInterface
     */
    public function setUpdated(string $updated): UpdatedResourceInterface;

    /**
     * @param \Visma\AkeneoIntegration\Api\Data\AssociationsInterface[] $associations
     * @return UpdatedResourceInterface
     */
    public function setAssociations(array $associations): UpdatedResourceInterface;

    /**
     * @param \Visma\AkeneoIntegration\Api\Data\QuantifiedAssociationsInterface $quantifiedAssociations
     * @return UpdatedResourceInterface
     */
    public function setQuantifiedAssociations(
        QuantifiedAssociationsInterface $quantifiedAssociations
    ): UpdatedResourceInterface;

    /**
     * @param \Visma\AkeneoIntegration\Api\Data\MetadataInterface $metadata
     * @return UpdatedResourceInterface
     */
    public function setMetadata(MetadataInterface $metadata): UpdatedResourceInterface;
}

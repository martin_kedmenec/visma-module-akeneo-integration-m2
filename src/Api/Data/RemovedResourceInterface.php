<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * @api
 */
interface RemovedResourceInterface extends ExtensibleDataInterface
{
    public const ACTION = 'action';

    public const EVENT_ID = 'event_id';

    public const EVENT_DATETIME = 'event_datetime';

    public const AUTHOR = 'author';

    public const AUTHOR_TYPE = 'author_type';

    public const PIM_SOURCE = 'pim_source';

    public const DATA = 'data';

    /**
     * @return string
     */
    public function getAction(): string;

    /**
     * @return string
     */
    public function getEventId(): string;

    /**
     * @return string
     */
    public function getEventDatetime(): string;

    /**
     * @return string
     */
    public function getAuthor(): string;

    /**
     * @return string
     */
    public function getAuthorType(): string;

    /**
     * @return string
     */
    public function getPimSource(): string;

    /**
     * @return UpdatedDataInterface
     */
    public function getResourceData(): UpdatedDataInterface;

    /**
     * @param string $action
     * @return RemovedResourceInterface
     */
    public function setAction(string $action): RemovedResourceInterface;

    /**
     * @param string $eventId
     * @return RemovedResourceInterface
     */
    public function setEventId(string $eventId): RemovedResourceInterface;

    /**
     * @param string $eventDatetime
     * @return RemovedResourceInterface
     */
    public function setEventDatetime(string $eventDatetime): RemovedResourceInterface;

    /**
     * @param string $author
     * @return RemovedResourceInterface
     */
    public function setAuthor(string $author): RemovedResourceInterface;

    /**
     * @param string $authorType
     * @return RemovedResourceInterface
     */
    public function setAuthorType(string $authorType): RemovedResourceInterface;

    /**
     * @param string $pimSource
     * @return RemovedResourceInterface
     */
    public function setPimSource(string $pimSource): RemovedResourceInterface;

    /**
     * @param UpdatedDataInterface $data
     * @return RemovedResourceInterface
     */
    public function setResourceData(UpdatedDataInterface $data): RemovedResourceInterface;
}

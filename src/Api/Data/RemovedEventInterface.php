<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * @api
 */
interface RemovedEventInterface extends ExtensibleDataInterface
{
    public const RESOURCE = 'resource';

    /**
     * @return RemovedResourceInterface
     */
    public function getResource(): RemovedResourceInterface;

    /**
     * @param RemovedResourceInterface $resource
     * @return RemovedEventInterface
     */
    public function setResource(RemovedResourceInterface $resource): RemovedEventInterface;
}

<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * @api
 */
interface AssociationsInterface extends ExtensibleDataInterface
{
    public const GROUPS = 'groups';

    public const PRODUCTS = 'products';

    public const PRODUCT_MODELS = 'product_models';

    /**
     * @return string[]
     */
    public function getGroups(): array;

    /**
     * @return string[]
     */
    public function getProducts(): array;

    /**
     * @return string[]
     */
    public function getProductModels(): array;

    /**
     * @param string[] $groups
     * @return AssociationsInterface
     */
    public function setGroups(array $groups): AssociationsInterface;

    /**
     * @param string[] $products
     * @return AssociationsInterface
     */
    public function setProducts(array $products): AssociationsInterface;

    /**
     * @param string[] $productModels
     * @return AssociationsInterface
     */
    public function setProductModels(array $productModels): AssociationsInterface;
}

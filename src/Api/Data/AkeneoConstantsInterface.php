<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Api\Data;

interface AkeneoConstantsInterface
{
    public const IS_AKENEO_ATTRIBUTE = 'is_akeneo_attribute';

    // TODO Find proper frontend types for
    // pim_catalog_image, pim_catalog_file, akeneo_reference_entity, akeneo_reference_entity_collection, pim_catalog_table
    public const AKENEO_MAGENTO_ATTRIBUTE_TYPE_MAPPING = [
        'pim_catalog_identifier' => 'text',
        'pim_catalog_text' => 'text',
        'pim_catalog_textarea' => 'textarea',
        'pim_catalog_simpleselect' => 'select',
        'pim_catalog_multiselect' => 'multiselect',
        'pim_catalog_boolean' => 'boolean',
        'pim_catalog_date' => 'date',
        'pim_catalog_number' => 'text',
        'pim_catalog_metric' => 'text',
        'pim_catalog_price_collection' => 'price',
        'pim_catalog_image' => 'text',
        'pim_catalog_file' => 'text',
        'pim_catalog_asset_collection' => 'textarea',
        'akeneo_reference_entity' => 'text',
        'akeneo_reference_entity_collection' => 'text',
        'pim_reference_data_simpleselect' => 'select',
        'pim_reference_data_multiselect' => 'multiselect',
        'pim_catalog_table' => 'text'
    ];
}

<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * @api
 */
interface MetadataInterface extends ExtensibleDataInterface
{
    public const WORKFLOW_STATUS = 'workflow_status';

    /**
     * @return string
     */
    public function getWorkflowStatus(): string;

    /**
     * @param string $workflowStatus
     * @return MetadataInterface
     */
    public function setWorkflowStatus(string $workflowStatus): MetadataInterface;
}

<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * @api
 */
interface ProductModelsInterface extends ExtensibleDataInterface
{
    public const CODE = 'code';

    public const QUANTITY = 'quantity';

    /**
     * @return string
     */
    public function getCode(): string;

    /**
     * @return int
     */
    public function getQuantity(): int;

    /**
     * @param string $code
     * @return ProductModelsInterface
     */
    public function setCode(string $code): ProductModelsInterface;

    /**
     * @param int $quantity
     * @return ProductModelsInterface
     */
    public function setQuantity(int $quantity): ProductModelsInterface;
}

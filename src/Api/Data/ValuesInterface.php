<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * @api
 */
interface ValuesInterface extends ExtensibleDataInterface
{
    public const ATTRIBUTE_CODE = 'attributeCode';

    /**
     * @return AttributeCodeInterface[]
     */
    public function getAttributeCode(): array;

    /**
     * @param AttributeCodeInterface[] $attributeCode
     * @return ValuesInterface
     */
    public function setAttributeCode(array $attributeCode): ValuesInterface;
}

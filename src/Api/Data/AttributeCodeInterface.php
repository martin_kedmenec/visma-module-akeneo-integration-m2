<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * @api
 */
interface AttributeCodeInterface extends ExtensibleDataInterface
{
    public const SCOPE = 'scope';

    public const LOCALE = 'locale';

    public const DATA = 'data';

    /**
     * @return string
     */
    public function getScope(): string;

    /**
     * @return string
     */
    public function getLocale(): string;

    /**
     * @return object
     */
    public function getAttributeData(): object;

    /**
     * @param string $scope
     * @return AttributeCodeInterface
     */
    public function setScope(string $scope): AttributeCodeInterface;

    /**
     * @param string $locale
     * @return AttributeCodeInterface
     */
    public function setLocale(string $locale): AttributeCodeInterface;

    /**
     * @param object $attributeData
     * @return AttributeCodeInterface
     */
    public function setAttributeData(object $attributeData): AttributeCodeInterface;
}

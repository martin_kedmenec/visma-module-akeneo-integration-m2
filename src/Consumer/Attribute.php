<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Consumer;

use Magento\AsynchronousOperations\Api\Data\OperationInterface;
use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Catalog\Api\Data\ProductAttributeInterfaceFactory;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\Eav\Api\Data\AttributeFrontendLabelInterface;
use Magento\Eav\Api\Data\AttributeFrontendLabelInterfaceFactory;
use Magento\Framework\Bulk\OperationInterface as BulkOperationInterface;
use Magento\Framework\Bulk\OperationManagementInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Serialize\Serializer\Json;
use Visma\AkeneoIntegration\Api\ConsumerInterface;
use Visma\AkeneoIntegration\Helper\AttributeTypeMapper;
use Visma\AkeneoIntegration\Helper\LocaleResolver;
use Visma\AkeneoIntegration\Logger\AkeneoIntegrationLogger;

class Attribute extends AbstractConsumer implements ConsumerInterface
{
    /**
     * @var ProductAttributeInterfaceFactory $productAttributeFactory
     */
    private ProductAttributeInterfaceFactory $productAttributeFactory;

    /**
     * @var AttributeFrontendLabelInterfaceFactory $attributeFrontendLabelFactory
     */
    private AttributeFrontendLabelInterfaceFactory $attributeFrontendLabelFactory;

    /**
     * @var ProductAttributeRepositoryInterface $productAttributeRepository
     */
    private ProductAttributeRepositoryInterface $productAttributeRepository;

    /**
     * @var LocaleResolver $localeResolver
     */
    private LocaleResolver $localeResolver;

    /**
     * @var AttributeTypeMapper $attributeTypeMapper
     */
    private AttributeTypeMapper $attributeTypeMapper;

    /**
     * @param ProductAttributeInterfaceFactory $productAttributeFactory
     * @param AttributeFrontendLabelInterfaceFactory $attributeFrontendLabelFactory
     * @param ProductAttributeRepositoryInterface $productAttributeRepository
     * @param LocaleResolver $localeResolver
     * @param AttributeTypeMapper $attributeTypeMapper
     * @param OperationManagementInterface $operationManagement
     * @param AkeneoIntegrationLogger $akeneoIntegrationLogger
     * @param Json $json
     */
    public function __construct(
        ProductAttributeInterfaceFactory $productAttributeFactory,
        AttributeFrontendLabelInterfaceFactory $attributeFrontendLabelFactory,
        ProductAttributeRepositoryInterface $productAttributeRepository,
        LocaleResolver $localeResolver,
        AttributeTypeMapper $attributeTypeMapper,
        OperationManagementInterface $operationManagement,
        AkeneoIntegrationLogger $akeneoIntegrationLogger,
        Json $json
    ) {
        $this->productAttributeFactory = $productAttributeFactory;
        $this->attributeFrontendLabelFactory = $attributeFrontendLabelFactory;
        $this->productAttributeRepository = $productAttributeRepository;
        $this->localeResolver = $localeResolver;
        $this->attributeTypeMapper = $attributeTypeMapper;
        parent::__construct(
            $operationManagement,
            $akeneoIntegrationLogger,
            $json
        );
    }

    /**
     * @inheritDoc
     */
    public function process(OperationInterface $operation): void
    {
        $this->akeneoIntegrationLogger->debug(
            __METHOD__ . " says: Operation received with UUID: {$operation->getBulkUuid()}, data: {$operation->getSerializedData()}"
        );

        $unserializedData = $this->unserializeData($operation);

        if (!$this->isExistingAttribute($unserializedData['code'])) {
            $this->createAttribute($unserializedData);
        }

        $this->operationManagement->changeOperationStatus(
            $operation->getBulkUuid(),
            $operation->getId(),
            BulkOperationInterface::STATUS_TYPE_COMPLETE
        );
    }

    /**
     * @param string $attributeCode
     * @return bool
     */
    private function isExistingAttribute(string $attributeCode): bool
    {
        try {
            $this->productAttributeRepository->get($attributeCode);
        } catch (NoSuchEntityException $exception) {
            return false;
        }

        $this->akeneoIntegrationLogger->notice(
            __METHOD__ . " says: The attribute code '$attributeCode' already exists and will not be processed."
        );

        return true;
    }

    /**
     * @param array $unserializedData
     * @return void
     */
    private function createAttribute(array $unserializedData): void
    {
        $attribute = $this->productAttributeFactory->create()
            ->setAttributeCode(strtolower($unserializedData['code']))
            ->setDefaultFrontendLabel("Akeneo {$unserializedData['code']}")
            ->setFrontendLabels($this->createFrontendLabels($unserializedData['labels']))
            ->setFrontendInput(
                $this->attributeTypeMapper->getFrontendInputTypeByAkeneoAttributeType($unserializedData['type'])
            )
            ->setCustomAttribute(self::IS_AKENEO_ATTRIBUTE, true);

        $this->saveAttribute($attribute);

        $this->akeneoIntegrationLogger->debug(
            __METHOD__ . " says: Attribute '{$attribute->getAttributeCode()}' created."
        );
    }

    /**
     * @param array $attributeLabels
     * @return AttributeFrontendLabelInterface[]
     */
    private function createFrontendLabels(array $attributeLabels): array
    {
        $frontendLabels = [];

        // TODO Decide what to do if $locale is not a locale currently used in Magento
        foreach ($attributeLabels as $locale => $label) {
            $frontendLabel = $this->attributeFrontendLabelFactory->create()
                ->setStoreId($this->localeResolver->getStoreIdByLocale($locale))
                ->setLabel($label);

            $frontendLabels[] = $frontendLabel;
        }

        return $frontendLabels;
    }

    /**
     * @param ProductAttributeInterface $attribute
     * @return void
     */
    private function saveAttribute(ProductAttributeInterface $attribute): void
    {
        try {
            $this->productAttributeRepository->save($attribute);
        } catch (CouldNotSaveException $exception) {
            $this->akeneoIntegrationLogger->critical($exception->getMessage());
        }
    }
}

<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Consumer;

use Magento\AsynchronousOperations\Api\Data\OperationInterface;
use Magento\Framework\Bulk\OperationManagementInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Visma\AkeneoIntegration\Api\Data\AkeneoConstantsInterface;
use Visma\AkeneoIntegration\Logger\AkeneoIntegrationLogger;

abstract class AbstractConsumer implements AkeneoConstantsInterface
{
    /**
     * @var OperationManagementInterface $operationManagement
     */
    protected OperationManagementInterface $operationManagement;

    /**
     * @var AkeneoIntegrationLogger $akeneoIntegrationLogger
     */
    protected AkeneoIntegrationLogger $akeneoIntegrationLogger;

    /**
     * @var Json $json
     */
    private Json $json;

    /**
     * @param OperationManagementInterface $operationManagement
     * @param AkeneoIntegrationLogger $akeneoIntegrationLogger
     * @param Json $json
     */
    public function __construct(
        OperationManagementInterface $operationManagement,
        AkeneoIntegrationLogger $akeneoIntegrationLogger,
        Json $json
    ) {
        $this->operationManagement = $operationManagement;
        $this->akeneoIntegrationLogger = $akeneoIntegrationLogger;
        $this->json = $json;
    }

    /**
     * @param OperationInterface $operation
     * @return array
     */
    protected function unserializeData(OperationInterface $operation): array
    {
        return $this->json->unserialize($operation->getSerializedData());
    }
}

<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Consumer;

use Magento\AsynchronousOperations\Api\Data\OperationInterface;
use Magento\Catalog\Api\AttributeSetRepositoryInterface;
use Magento\Catalog\Api\ProductAttributeGroupRepositoryInterface;
use Magento\Catalog\Api\ProductAttributeManagementInterface;
use Magento\Eav\Api\Data\AttributeGroupInterface;
use Magento\Eav\Api\Data\AttributeGroupInterfaceFactory;
use Magento\Eav\Api\Data\AttributeSetInterface;
use Magento\Eav\Api\Data\AttributeSetInterfaceFactory;
use Magento\Framework\Bulk\OperationInterface as BulkOperationInterface;
use Magento\Framework\Bulk\OperationManagementInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Serialize\Serializer\Json;
use Visma\AkeneoIntegration\Api\ConsumerInterface;
use Visma\AkeneoIntegration\Logger\AkeneoIntegrationLogger;

class Family extends AbstractConsumer implements ConsumerInterface
{
    /**
     * @var AttributeSetInterfaceFactory $attributeSetFactory
     */
    private AttributeSetInterfaceFactory $attributeSetFactory;

    /**
     * @var AttributeGroupInterfaceFactory $attributeGroupFactory
     */
    private AttributeGroupInterfaceFactory $attributeGroupFactory;

    /**
     * @var AttributeSetRepositoryInterface $attributeSetRepository
     */
    private AttributeSetRepositoryInterface $attributeSetRepository;

    /**
     * @var ProductAttributeGroupRepositoryInterface $productAttributeGroupRepository
     */
    private ProductAttributeGroupRepositoryInterface $productAttributeGroupRepository;

    /**
     * @var ProductAttributeManagementInterface $productAttributeManagement
     */
    private ProductAttributeManagementInterface $productAttributeManagement;

    /**
     * @param AttributeSetInterfaceFactory $attributeSetFactory
     * @param AttributeGroupInterfaceFactory $attributeGroupFactory
     * @param AttributeSetRepositoryInterface $attributeSetRepository
     * @param ProductAttributeGroupRepositoryInterface $productAttributeGroupRepository
     * @param ProductAttributeManagementInterface $productAttributeManagement
     * @param OperationManagementInterface $operationManagement
     * @param AkeneoIntegrationLogger $akeneoIntegrationLogger
     * @param Json $json
     */
    public function __construct(
        AttributeSetInterfaceFactory $attributeSetFactory,
        AttributeGroupInterfaceFactory $attributeGroupFactory,
        AttributeSetRepositoryInterface $attributeSetRepository,
        ProductAttributeGroupRepositoryInterface $productAttributeGroupRepository,
        ProductAttributeManagementInterface $productAttributeManagement,
        OperationManagementInterface $operationManagement,
        AkeneoIntegrationLogger $akeneoIntegrationLogger,
        Json $json
    ) {
        $this->attributeSetFactory = $attributeSetFactory;
        $this->attributeGroupFactory = $attributeGroupFactory;
        $this->attributeSetRepository = $attributeSetRepository;
        $this->productAttributeGroupRepository = $productAttributeGroupRepository;
        $this->productAttributeManagement = $productAttributeManagement;
        parent::__construct(
            $operationManagement,
            $akeneoIntegrationLogger,
            $json
        );
    }

    /**
     * @inheritDoc
     */
    public function process(OperationInterface $operation): void
    {
        $this->akeneoIntegrationLogger->debug(
            __METHOD__ . " says: Operation received with UUID: {$operation->getBulkUuid()}, data: {$operation->getSerializedData()}"
        );

        $unserializedData = $this->unserializeData($operation);

        $attributeSet = $this->createAttributeSet($unserializedData);
        $attributeGroup = $this->createAkeneoAttributeGroup((int)$attributeSet->getAttributeSetId());

        $this->assignAttributesToAttributeSet(
            $attributeSet,
            (int)$attributeGroup->getAttributeGroupId(),
            $unserializedData['attributes']
        );

        $this->operationManagement->changeOperationStatus(
            $operation->getBulkUuid(),
            $operation->getId(),
            BulkOperationInterface::STATUS_TYPE_COMPLETE
        );
    }

    /**
     * @param array $unserializedData
     * @return AttributeSetInterface
     */
    private function createAttributeSet(array $unserializedData): AttributeSetInterface
    {
        $attributeSet = $this->attributeSetFactory->create()
            ->setEntityTypeId(4)
            ->setAttributeSetName("Akeneo {$unserializedData['code']}");

        $this->saveAttributeSet($attributeSet);
        $attributeSet->initFromSkeleton(4);

        return $this->saveAttributeSet($attributeSet);
    }

    /**
     * @param AttributeSetInterface $attributeSet
     * @return AttributeSetInterface
     */
    private function saveAttributeSet(AttributeSetInterface $attributeSet): AttributeSetInterface
    {
        try {
            $this->attributeSetRepository->save($attributeSet);
        } catch (CouldNotSaveException $exception) {
            $this->akeneoIntegrationLogger->critical($exception->getMessage());
        }

        return $attributeSet;
    }

    /**
     * @param int $attributeSetId
     * @return AttributeGroupInterface
     */
    private function createAkeneoAttributeGroup(int $attributeSetId): AttributeGroupInterface
    {
        $akeneoAttributeGroup = $this->attributeGroupFactory->create()
            ->setAttributeGroupName('Akeneo')
            ->setAttributeSetId($attributeSetId);

        return $this->saveAkeneoAttributeGroup($akeneoAttributeGroup);
    }

    /**
     * @param AttributeGroupInterface $attributeGroup
     * @return AttributeGroupInterface
     */
    private function saveAkeneoAttributeGroup(AttributeGroupInterface $attributeGroup): AttributeGroupInterface
    {
        try {
            $this->productAttributeGroupRepository->save($attributeGroup);
        } catch (CouldNotSaveException $exception) {
            $this->akeneoIntegrationLogger->critical($exception->getMessage());
        }

        return $attributeGroup;
    }

    /**
     * @param AttributeSetInterface $attributeSet
     * @param int $attributeGroupId
     * @param array $attributes
     * @return void
     */
    private function assignAttributesToAttributeSet(
        AttributeSetInterface $attributeSet,
        int $attributeGroupId,
        array $attributes
    ): void {
        $sortOrder = 0;

        foreach ($attributes as $attribute) {
            $this->productAttributeManagement->assign(
                $attributeSet->getAttributeSetId(),
                $attributeGroupId,
                $attribute,
                $sortOrder
            );

            $sortOrder++;
        }
    }
}

<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Rest;

use Akeneo\Pim\ApiClient\AkeneoPimClientBuilder;
use Akeneo\Pim\ApiClient\AkeneoPimClientBuilderFactory;
use Akeneo\Pim\ApiClient\AkeneoPimClientInterface;
use Visma\AkeneoIntegration\Helper\Config;
use Visma\AkeneoIntegration\Logger\AkeneoIntegrationLogger;

abstract class AbstractClient
{
    /**
     * @var AkeneoPimClientBuilderFactory $akeneoPimClientBuilderFactory
     */
    private AkeneoPimClientBuilderFactory $akeneoPimClientBuilderFactory;

    /**
     * @var Config $config
     */
    private Config $config;

    /**
     * @var AkeneoIntegrationLogger $akeneoIntegrationLogger
     */
    protected AkeneoIntegrationLogger $akeneoIntegrationLogger;

    /**
     * @param AkeneoPimClientBuilderFactory $akeneoPimClientBuilderFactory
     * @param Config $config
     * @param AkeneoIntegrationLogger $akeneoIntegrationLogger
     */
    public function __construct(
        AkeneoPimClientBuilderFactory $akeneoPimClientBuilderFactory,
        Config $config,
        AkeneoIntegrationLogger $akeneoIntegrationLogger
    ) {
        $this->akeneoPimClientBuilderFactory = $akeneoPimClientBuilderFactory;
        $this->config = $config;
        $this->akeneoIntegrationLogger = $akeneoIntegrationLogger;
    }

    /**
     * @return AkeneoPimClientInterface
     */
    protected function createClient(): AkeneoPimClientInterface
    {
        // TODO Figure out how to use the client factory
        /*$akeneoPimClientBuilder = $this->akeneoPimClientBuilderFactory->create(
            ['base_uri' => $this->config->getConnectionBaseUrl()]
        );*/

        $akeneoPimClientBuilder = new AkeneoPimClientBuilder($this->config->getConnectionBaseUrl());

        return $akeneoPimClientBuilder->buildAuthenticatedByPassword(
            $this->config->getConnectionClientId(),
            $this->config->getConnectionSecret(),
            $this->config->getConnectionUsername(),
            $this->config->getConnectionPassword()
        );
    }
}

<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Rest;

use Akeneo\Pim\ApiClient\Exception\NotFoundHttpException;

class FamilyApi extends AbstractClient
{
    /**
     * @return array
     */
    public function getAllFamilies(): array
    {
        $families = [];

        try {
            foreach ($this->createClient()->getFamilyApi()->all() as $family) {
                $families[] = $family;
            }
        } catch (NotFoundHttpException $exception) {
            $this->akeneoIntegrationLogger->error($exception->getMessage());
        }

        return $families;
    }
}

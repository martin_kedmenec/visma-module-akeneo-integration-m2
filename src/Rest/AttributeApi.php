<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Rest;

use Akeneo\Pim\ApiClient\Exception\NotFoundHttpException;

class AttributeApi extends AbstractClient
{
    /**
     * @param string $attributeCode
     * @return array
     */
    public function getAttributeByAttributeCode(string $attributeCode): array
    {
        $response = [];

        try {
            $response = $this->createClient()->getAttributeApi()->get($attributeCode);
        } catch (NotFoundHttpException $exception) {
            $this->akeneoIntegrationLogger->error($exception->getMessage());
        }

        return $response;
    }

    /**
     * @return array
     */
    public function getAllAttributes(): array
    {
        $attributes = [];

        try {
            foreach ($this->createClient()->getAttributeApi()->all() as $attribute) {
                $attributes[] = $attribute;
            }
        } catch (NotFoundHttpException $exception) {
            $this->akeneoIntegrationLogger->error($exception->getMessage());
        }

        return $attributes;
    }
}

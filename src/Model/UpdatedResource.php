<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Model;

use Magento\Framework\DataObject;
use Visma\AkeneoIntegration\Api\Data\MetadataInterface;
use Visma\AkeneoIntegration\Api\Data\QuantifiedAssociationsInterface;
use Visma\AkeneoIntegration\Api\Data\UpdatedResourceInterface;

/**
 * TODO Implement other model interfaces
 */
class UpdatedResource extends DataObject implements UpdatedResourceInterface
{
    /**
     * @inheritDoc
     */
    public function getIdentifier(): string
    {
        return $this->getData(self::IDENTIFIER);
    }

    /**
     * @inheritDoc
     */
    public function isEnabled(): bool
    {
        return $this->getData(self::ENABLED);
    }

    /**
     * @inheritDoc
     */
    public function getFamily(): string
    {
        return $this->getData(self::FAMILY);
    }

    /**
     * @inheritDoc
     */
    public function getCategories(): array
    {
        return $this->getData(self::CATEGORIES);
    }

    /**
     * @inheritDoc
     */
    public function getGroups(): ?array
    {
        return $this->getData(self::GROUPS);
    }

    /**
     * @inheritDoc
     */
    public function getParent(): ?string
    {
        return $this->getData(self::PARENT);
    }

    /**
     * @inheritDoc
     */
    public function getValues()
    {
        return $this->getData(self::VALUES);
    }

    /**
     * @inheritDoc
     */
    public function getCreated(): string
    {
        return $this->getData(self::CREATED);
    }

    /**
     * @inheritDoc
     */
    public function getUpdated(): string
    {
        return $this->getData(self::UPDATED);
    }

    /**
     * @inheritDoc
     */
    public function getAssociations(): array
    {
        return $this->getData(self::ASSOCIATIONS);
    }

    /**
     * @inheritDoc
     */
    public function getQuantifiedAssociations(): QuantifiedAssociationsInterface
    {
        return $this->getData(self::QUANTIFIED_ASSOCIATIONS);
    }

    /**
     * @inheritDoc
     */
    public function getMetadata(): MetadataInterface
    {
        return $this->getData(self::METADATA);
    }

    /**
     * @inheritDoc
     */
    public function setIdentifier(string $identifier): UpdatedResourceInterface
    {
        return $this->setData(self::IDENTIFIER, $identifier);
    }

    /**
     * @inheritDoc
     */
    public function setEnabled(bool $enabled): UpdatedResourceInterface
    {
        return $this->setData(self::ENABLED, $enabled);
    }

    /**
     * @inheritDoc
     */
    public function setFamily(string $family): UpdatedResourceInterface
    {
        return $this->setData(self::FAMILY, $family);
    }

    /**
     * @inheritDoc
     */
    public function setCategories(array $categories): UpdatedResourceInterface
    {
        return $this->setData(self::CATEGORIES, $categories);
    }

    /**
     * @inheritDoc
     */
    public function setGroups(?array $groups): UpdatedResourceInterface
    {
        return $this->setData(self::GROUPS, $groups);
    }

    /**
     * @inheritDoc
     */
    public function setParent(?string $parent): UpdatedResourceInterface
    {
        return $this->setData(self::PARENT, $parent);
    }

    /**
     * @inheritDoc
     */
    public function setValues($values): UpdatedResourceInterface
    {
        return $this->setData(self::VALUES, $values);
    }

    /**
     * @inheritDoc
     */
    public function setCreated(string $created): UpdatedResourceInterface
    {
        return $this->setData(self::CREATED, $created);
    }

    /**
     * @inheritDoc
     */
    public function setUpdated(string $updated): UpdatedResourceInterface
    {
        return $this->setData(self::UPDATED, $updated);
    }

    /**
     * @inheritDoc
     */
    public function setAssociations(array $associations): UpdatedResourceInterface
    {
        return $this->setData(self::ASSOCIATIONS, $associations);
    }

    /**
     * @inheritDoc
     */
    public function setQuantifiedAssociations(
        QuantifiedAssociationsInterface $quantifiedAssociations
    ): UpdatedResourceInterface {
        return $this->setData(self::QUANTIFIED_ASSOCIATIONS, $quantifiedAssociations);
    }

    /**
     * @inheritDoc
     */
    public function setMetadata(MetadataInterface $metadata): UpdatedResourceInterface
    {
        return $this->setData(self::METADATA, $metadata);
    }
}

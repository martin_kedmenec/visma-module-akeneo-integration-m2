<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Model;

use Magento\Framework\DataObject;
use Visma\AkeneoIntegration\Api\Data\QuantifiedAssociationsInterface;

/**
 * TODO Implement other model interfaces
 */
class QuantifiedAssociations extends DataObject implements QuantifiedAssociationsInterface
{
    /**
     * @inheritDoc
     */
    public function getProducts(): ?array
    {
        return $this->getData(self::PRODUCTS);
    }

    /**
     * @inheritDoc
     */
    public function getProductModels(): ?array
    {
        return $this->getData(self::PRODUCT_MODELS);
    }

    /**
     * @inheritDoc
     */
    public function setProducts(?array $products): QuantifiedAssociationsInterface
    {
        return $this->setData(self::PRODUCTS, $products);
    }

    /**
     * @inheritDoc
     */
    public function setProductModels(?array $productModels): QuantifiedAssociationsInterface
    {
        return $this->setData(self::PRODUCT_MODELS, $productModels);
    }
}

<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Model;

use Magento\Framework\DataObject;
use Visma\AkeneoIntegration\Api\Data\MetadataInterface;

/**
 * TODO Implement other model interfaces
 */
class Metadata extends DataObject implements MetadataInterface
{
    /**
     * @inheritDoc
     */
    public function getWorkflowStatus(): string
    {
        return $this->getData(self::WORKFLOW_STATUS);
    }

    /**
     * @inheritDoc
     */
    public function setWorkflowStatus(string $workflowStatus): MetadataInterface
    {
        return $this->setData(self::WORKFLOW_STATUS, $workflowStatus);
    }
}

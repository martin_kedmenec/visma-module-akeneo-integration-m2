<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Model;

use Magento\Framework\DataObject;
use Visma\AkeneoIntegration\Api\Data\AttributeCodeInterface;

/**
 * TODO Implement other model interfaces
 */
class AttributeCode extends DataObject implements AttributeCodeInterface
{
    /**
     * @inheritDoc
     */
    public function getScope(): string
    {
        return $this->getData(self::SCOPE);
    }

    /**
     * @inheritDoc
     */
    public function getLocale(): string
    {
        return $this->getData(self::LOCALE);
    }

    /**
     * @inheritDoc
     */
    public function getAttributeData(): object
    {
        return $this->getData(self::DATA);
    }

    /**
     * @inheritDoc
     */
    public function setScope(string $scope): AttributeCodeInterface
    {
        return $this->setData(self::SCOPE, $scope);
    }

    /**
     * @inheritDoc
     */
    public function setLocale(string $locale): AttributeCodeInterface
    {
        return $this->setData(self::LOCALE, $locale);
    }

    /**
     * @inheritDoc
     */
    public function setAttributeData(object $attributeData): AttributeCodeInterface
    {
        return $this->setData(self::DATA, $attributeData);
    }
}

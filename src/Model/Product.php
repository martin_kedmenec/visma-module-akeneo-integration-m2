<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Model;

use Magento\Framework\DataObject;
use Visma\AkeneoIntegration\Api\Data\ProductInterface;

/**
 * TODO Implement other model interfaces
 */
class Product extends DataObject implements ProductInterface
{
    /**
     * @inheritDoc
     */
    public function getIdentifier(): string
    {
        return $this->getData(self::IDENTIFIER);
    }

    /**
     * @inheritDoc
     */
    public function getQuantity(): int
    {
        return $this->getData(self::QUANTITY);
    }

    /**
     * @inheritDoc
     */
    public function setIdentifier(string $identifier): ProductInterface
    {
        return $this->setData(self::IDENTIFIER, $identifier);
    }

    /**
     * @inheritDoc
     */
    public function setQuantity(int $quantity): ProductInterface
    {
        return $this->setData(self::QUANTITY, $quantity);
    }
}

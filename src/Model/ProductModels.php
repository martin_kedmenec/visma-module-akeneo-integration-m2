<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Model;

use Magento\Framework\DataObject;
use Visma\AkeneoIntegration\Api\Data\ProductModelsInterface;

/**
 * TODO Implement other model interfaces
 */
class ProductModels extends DataObject implements ProductModelsInterface
{
    /**
     * @inheritDoc
     */
    public function getCode(): string
    {
        return $this->getData(self::CODE);
    }

    /**
     * @inheritDoc
     */
    public function getQuantity(): int
    {
        return $this->getData(self::QUANTITY);
    }

    /**
     * @inheritDoc
     */
    public function setCode(string $code): ProductModelsInterface
    {
        return $this->setData(self::CODE, $code);
    }

    /**
     * @inheritDoc
     */
    public function setQuantity(int $quantity): ProductModelsInterface
    {
        return $this->setData(self::QUANTITY, $quantity);
    }
}

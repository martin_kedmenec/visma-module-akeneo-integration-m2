<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Model;

use Magento\Framework\DataObject;
use Visma\AkeneoIntegration\Api\Data\AssociationsInterface;

/**
 * TODO Implement other model interfaces
 */
class Associations extends DataObject implements AssociationsInterface
{
    /**
     * @inheritDoc
     */
    public function getGroups(): array
    {
        return $this->getData(self::GROUPS);
    }

    /**
     * @inheritDoc
     */
    public function getProducts(): array
    {
        return $this->getData(self::PRODUCTS);
    }

    /**
     * @inheritDoc
     */
    public function getProductModels(): array
    {
        return $this->getData(self::PRODUCT_MODELS);
    }

    /**
     * @inheritDoc
     */
    public function setGroups(array $groups): AssociationsInterface
    {
        return $this->setData(self::GROUPS, $groups);
    }

    /**
     * @inheritDoc
     */
    public function setProducts(array $products): AssociationsInterface
    {
        return $this->setData(self::PRODUCTS, $products);
    }

    /**
     * @inheritDoc
     */
    public function setProductModels(array $productModels): AssociationsInterface
    {
        return $this->setData(self::PRODUCT_MODELS, $productModels);
    }
}

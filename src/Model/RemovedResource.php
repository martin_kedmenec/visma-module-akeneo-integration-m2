<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Model;

use Magento\Framework\DataObject;
use Visma\AkeneoIntegration\Api\Data\RemovedResourceInterface;
use Visma\AkeneoIntegration\Api\Data\UpdatedDataInterface;

class RemovedResource extends DataObject implements RemovedResourceInterface
{
    /**
     * @inheritDoc
     */
    public function getAction(): string
    {
        return $this->getData(self::ACTION);
    }

    /**
     * @inheritDoc
     */
    public function getEventId(): string
    {
        return $this->getData(self::EVENT_ID);
    }

    /**
     * @inheritDoc
     */
    public function getEventDatetime(): string
    {
        return $this->getData(self::EVENT_DATETIME);
    }

    /**
     * @inheritDoc
     */
    public function getAuthor(): string
    {
        return $this->getData(self::AUTHOR);
    }

    /**
     * @inheritDoc
     */
    public function getAuthorType(): string
    {
        return $this->getData(self::AUTHOR_TYPE);
    }

    /**
     * @inheritDoc
     */
    public function getPimSource(): string
    {
        return $this->getData(self::PIM_SOURCE);
    }

    /**
     * @inheritDoc
     */
    public function getResourceData(): UpdatedDataInterface
    {
        return $this->getData(self::DATA);
    }

    /**
     * @inheritDoc
     */
    public function setAction(string $action): RemovedResourceInterface
    {
        return $this->setData(self::ACTION, $action);
    }

    /**
     * @inheritDoc
     */
    public function setEventId(string $eventId): RemovedResourceInterface
    {
        return $this->setData(self::EVENT_ID, $eventId);
    }

    /**
     * @inheritDoc
     */
    public function setEventDatetime(string $eventDatetime): RemovedResourceInterface
    {
        return $this->setData(self::EVENT_DATETIME, $eventDatetime);
    }

    /**
     * @inheritDoc
     */
    public function setAuthor(string $author): RemovedResourceInterface
    {
        return $this->setData(self::AUTHOR, $author);
    }

    /**
     * @inheritDoc
     */
    public function setAuthorType(string $authorType): RemovedResourceInterface
    {
        return $this->setData(self::AUTHOR_TYPE, $authorType);
    }

    /**
     * @inheritDoc
     */
    public function setPimSource(string $pimSource): RemovedResourceInterface
    {
        return $this->setData(self::PIM_SOURCE, $pimSource);
    }

    /**
     * @inheritDoc
     */
    public function setResourceData(UpdatedDataInterface $data): RemovedResourceInterface
    {
        return $this->setData(self::DATA, $data);
    }
}

<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Model;

use Magento\Framework\DataObject;
use Visma\AkeneoIntegration\Api\Data\UpdatedDataInterface;
use Visma\AkeneoIntegration\Api\Data\UpdatedResourceInterface;

/**
 * TODO Implement other model interfaces
 */
class UpdatedData extends DataObject implements UpdatedDataInterface
{
    /**
     * @inheritDoc
     */
    public function getResource(): UpdatedResourceInterface
    {
        return $this->getData(self::RESOURCE);
    }

    /**
     * @inheritDoc
     */
    public function setResource(UpdatedResourceInterface $resource): UpdatedDataInterface
    {
        return $this->setData(self::RESOURCE, $resource);
    }
}

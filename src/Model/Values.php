<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Model;

use Magento\Framework\DataObject;
use Visma\AkeneoIntegration\Api\Data\ValuesInterface;

/**
 * TODO Implement other model interfaces
 */
class Values extends DataObject implements ValuesInterface
{
    /**
     * @inheritDoc
     */
    public function getAttributeCode(): array
    {
        return $this->getData(self::ATTRIBUTE_CODE);
    }

    /**
     * @inheritDoc
     */
    public function setAttributeCode(array $attributeCode): ValuesInterface
    {
        return $this->setData(self::ATTRIBUTE_CODE, $attributeCode);
    }
}

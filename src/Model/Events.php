<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Model;

use Magento\Framework\DataObject;
use Visma\AkeneoIntegration\Api\Data\EventsInterface;

/**
 * TODO Implement other model interfaces
 */
class Events extends DataObject implements EventsInterface
{
    /**
     * @inheritDoc
     */
    public function getEvents(): array
    {
        return $this->getData(self::EVENTS);
    }

    /**
     * @inheritDoc
     */
    public function setEvents(array $events): EventsInterface
    {
        return $this->setData(self::EVENTS, $events);
    }
}

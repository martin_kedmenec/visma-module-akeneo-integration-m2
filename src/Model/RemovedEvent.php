<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Model;

use Magento\Framework\DataObject;
use Visma\AkeneoIntegration\Api\Data\RemovedEventInterface;
use Visma\AkeneoIntegration\Api\Data\RemovedResourceInterface;

class RemovedEvent extends DataObject implements RemovedEventInterface
{
    /**
     * @inheritDoc
     */
    public function getResource(): RemovedResourceInterface
    {
        return $this->getData(self::RESOURCE);
    }

    /**
     * @inheritDoc
     */
    public function setResource(RemovedResourceInterface $resource): RemovedEventInterface
    {
        return $this->setData(self::RESOURCE, $resource);
    }
}

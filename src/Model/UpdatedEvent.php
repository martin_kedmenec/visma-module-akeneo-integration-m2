<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Model;

use Visma\AkeneoIntegration\Api\Data\UpdatedDataInterface;
use Visma\AkeneoIntegration\Api\Data\UpdatedEventInterface;

/**
 * TODO Implement other model interfaces
 * This class doesn't extend from \Magento\Framework\DataObject because of the data key in the object
 */
class UpdatedEvent implements UpdatedEventInterface
{
    private $data = [];

    /**
     * @inheritDoc
     */
    public function getAction(): string
    {
        return $this->data[self::ACTION];
    }

    /**
     * @inheritDoc
     */
    public function getEventId(): string
    {
        return $this->data[self::EVENT_ID];
    }

    /**
     * @inheritDoc
     */
    public function getEventDatetime(): string
    {
        return $this->data[self::EVENT_DATETIME];
    }

    /**
     * @inheritDoc
     */
    public function getAuthor(): string
    {
        return $this->data[self::AUTHOR];
    }

    /**
     * @inheritDoc
     */
    public function getAuthorType(): string
    {
        return $this->data[self::AUTHOR_TYPE];
    }

    /**
     * @inheritDoc
     */
    public function getPimSource(): string
    {
        return $this->data[self::PIM_SOURCE];
    }

    /**
     * @inheritDoc
     */
    public function getData(): UpdatedDataInterface
    {
        return $this->data[self::DATA];
    }

    /**
     * @inheritDoc
     */
    public function setAction(string $action): UpdatedEventInterface
    {
        $this->data[self::ACTION] = $action;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setEventId(string $eventId): UpdatedEventInterface
    {
        $this->data[self::EVENT_ID] = $eventId;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setEventDatetime(string $eventDatetime): UpdatedEventInterface
    {
        $this->data[self::EVENT_DATETIME] = $eventDatetime;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setAuthor(string $author): UpdatedEventInterface
    {
        $this->data[self::AUTHOR] = $author;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setAuthorType(string $authorType): UpdatedEventInterface
    {
        $this->data[self::AUTHOR_TYPE] = $authorType;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setPimSource(string $pimSource): UpdatedEventInterface
    {
        $this->data[self::PIM_SOURCE] = $pimSource;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setData(UpdatedDataInterface $data): UpdatedEventInterface
    {
        $this->data[self::DATA] = $data;

        return $this;
    }
}

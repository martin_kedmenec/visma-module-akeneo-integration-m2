<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Controller\Adminhtml\Setup;

use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Message\ManagerInterface;
use Visma\AkeneoIntegration\Service\AttributeProcessor;
use Visma\AkeneoIntegration\Service\FamilyProcessor;

class Index implements ActionInterface
{
    /**
     * @var RedirectInterface $redirect
     */
    private RedirectInterface $redirect;

    /**
     * @var RedirectFactory $redirectFactory
     */
    private RedirectFactory $redirectFactory;

    /**
     * @var AttributeProcessor $attributeProcessor
     */
    private AttributeProcessor $attributeProcessor;

    /**
     * @var FamilyProcessor $familyProcessor
     */
    private FamilyProcessor $familyProcessor;

    /**
     * @var ManagerInterface $manager
     */
    private ManagerInterface $manager;

    /**
     * @param RedirectInterface $redirect
     * @param RedirectFactory $redirectFactory
     * @param AttributeProcessor $attributeProcessor
     * @param FamilyProcessor $familyProcessor
     * @param ManagerInterface $manager
     */
    public function __construct(
        RedirectInterface $redirect,
        RedirectFactory $redirectFactory,
        AttributeProcessor $attributeProcessor,
        FamilyProcessor $familyProcessor,
        ManagerInterface $manager
    ) {
        $this->redirect = $redirect;
        $this->redirectFactory = $redirectFactory;
        $this->attributeProcessor = $attributeProcessor;
        $this->familyProcessor = $familyProcessor;
        $this->manager = $manager;
    }

    /**
     * @inheritDoc
     * @psalm-suppress ImplicitToStringCast // Due to __() usage
     */
    public function execute(): ResultInterface
    {
        $this->attributeProcessor->process();
        $this->familyProcessor->process();

        $this->manager->addSuccessMessage(__('Setup started'));

        $redirect = $this->redirectFactory->create();

        return $redirect->setUrl($this->redirect->getRefererUrl());
    }
}

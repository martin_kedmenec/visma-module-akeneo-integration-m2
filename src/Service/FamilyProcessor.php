<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Service;

use Visma\AkeneoIntegration\Api\ProcessorServiceInterface;
use Visma\AkeneoIntegration\Publisher\Publisher;
use Visma\AkeneoIntegration\Rest\FamilyApi;

class FamilyProcessor implements ProcessorServiceInterface
{
    /**
     * @var FamilyApi $familyApi
     */
    private FamilyApi $familyApi;

    /**
     * @var Publisher $publisher
     */
    private Publisher $publisher;

    /**
     * @param FamilyApi $familyApi
     * @param Publisher $publisher
     */
    public function __construct(
        FamilyApi $familyApi,
        Publisher $publisher
    ) {
        $this->familyApi = $familyApi;
        $this->publisher = $publisher;
    }

    /**
     * @inheritDoc
     */
    public function process(): void
    {
        foreach ($this->familyApi->getAllFamilies() as $family) {
            $this->publisher->execute($family, 'visma.akeneo.family.process');
        }
    }
}

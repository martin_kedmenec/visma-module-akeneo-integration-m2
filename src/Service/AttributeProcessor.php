<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Service;

use Visma\AkeneoIntegration\Api\ProcessorServiceInterface;
use Visma\AkeneoIntegration\Publisher\Publisher;
use Visma\AkeneoIntegration\Rest\AttributeApi;

class AttributeProcessor implements ProcessorServiceInterface
{
    /**
     * @var AttributeApi $attributeApi
     */
    private AttributeApi $attributeApi;

    /**
     * @var Publisher $publisher
     */
    private Publisher $publisher;

    /**
     * @param AttributeApi $attributeApi
     * @param Publisher $publisher
     */
    public function __construct(
        AttributeApi $attributeApi,
        Publisher $publisher
    ) {
        $this->attributeApi = $attributeApi;
        $this->publisher = $publisher;
    }

    /**
     * @inheritDoc
     */
    public function process(): void
    {
        foreach ($this->attributeApi->getAllAttributes() as $attribute) {
            $this->publisher->execute($attribute, 'visma.akeneo.attribute.process');
        }
    }
}

<?php

declare(strict_types=1);

namespace Visma\AkeneoIntegration\Service;

use Magento\Framework\Serialize\Serializer\Json;
use Visma\AkeneoIntegration\Api\Data\UpdatedEventInterface;
use Visma\AkeneoIntegration\Api\EventsProcessorInterface;
use Visma\AkeneoIntegration\Logger\AkeneoIntegrationLogger;

class EventsProcessor implements EventsProcessorInterface
{
    /**
     * @var AkeneoIntegrationLogger $akeneoIntegrationLogger
     */
    private AkeneoIntegrationLogger $akeneoIntegrationLogger;

    /**
     * @var Json $json
     */
    private Json $json;

    /**
     * @param AkeneoIntegrationLogger $akeneoIntegrationLogger
     * @param Json $json
     */
    public function __construct(
        AkeneoIntegrationLogger $akeneoIntegrationLogger,
        Json $json
    ) {
        $this->akeneoIntegrationLogger = $akeneoIntegrationLogger;
        $this->json = $json;
    }

    /**
     * @inheirtDoc
     */
    public function process(array $events): UpdatedEventInterface
    {
        $jsonString = $this->json->serialize($events[0]->getData()->getResource()->getValues());

        $this->akeneoIntegrationLogger->debug($jsonString);

        return $events[0];
    }
}

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.1] - 2021-12-18

### Added

- Module added

[Unreleased]:

[0.0.1]: https://bitbucket.org/martin_kedmenec/visma-module-akeneo-integration-m2/src/master/0.0.1
